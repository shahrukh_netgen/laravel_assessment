<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;



class UserController extends Controller
{
    function login(Request $request){
        $valudator = $request->validate([
                 'username'=>'required',
                 'password'=>'required'
            ]);
            
         $username = $request->input('username');
         $password = $request->input('password');
         
         $check = DB::table('users')
                     ->where('username','=',$username)
                     ->first();
         if($check!=null){
             
                if(Hash::check($password,$check->password)){
                    $request->session()->put('USER_ID',$check->username);
                    $request->session()->put('USER_DP',$check->dp);
                    return redirect('/user/dashboard');
                    
                }else{
                 
                    return redirect('/user/login')->with('message','Password galat hai. Fir se Try kejiye.');
                }
             
             
         }else{
             
             
             return redirect('/user/login')->with('message','Please Try Again');
         }
     }

     function signup(Request $request){
        $valudator = $request->validate([
            'username'=>'required',
            'password'=>'required',
            'name'=>'required',
            'email'=>'required|email',
            'dob'=>'required',
            'phone'=>'required|min:10|max:12',
            'dp' => 'required|image|mimes:jpeg,png,jpg|max:1024',
       ]);
       
    $username = $request->input('username');
    $password = $request->input('password');
    $name = $request->input('name');
    $email = $request->input('email');
    $dob = $request->input('dob');
    $phone = $request->input('phone');
    $permitted_chars = '0123456789';
    $uniqueString = substr(str_shuffle($permitted_chars), 0, 6);
    $imageName = time().'.'.$request->dp->extension();  
    $url = $request->dp->move(public_path('images'), $imageName);
   
    $img = Image::make($url);
    $img->resize(100, 100)->save();
    $checkUsername = DB::table('users')->where('username',$username)->count();
    $checkEmail = DB::table('users')->where('email',$email)->count();
    $checkPhone = DB::table('users')->where('phone',$phone)->count();
    $status = "0";

            if($checkUsername>0){
                return redirect('/user/signup')->with('message','Username Already Exists');
            }
            else if($checkEmail>0){
                return redirect('/user/signup')->with('message','Email Already Exists');
            }
            else if($checkPhone>0){
                return redirect('/user/signup')->with('message','Phone Number Already Exists');
            }else{

                $query = DB::table('users')->insert([
                            'username'=>$username,
                            'name'=>$name,
                            'email'=>$email,
                            'phone'=>$phone,
                            'dob'=>$dob,
                            'vCode'=>$uniqueString,
                            'status'=>$status,
                            'password'=>Hash::make($password),
                            'dp'=>$imageName    
                ]);
                if($query!=null){
                    

                    return redirect('/user/login')->with('message','Your Account Has Been Created. You Can Now Login!');
                }else{
                    return redirect('/user/signup')->with('message','Please Try Again!');
                }

            }
            


     }

     function fetchDashboardData(){
        

        
        $fetchTotalContactCount = DB::table('contact')->where('username','=',Session()->get('USER_ID'))->count();
        $fetchTotalVisitorsCount = DB::table('visitors')->where('username','=',Session()->get('USER_ID'))->count();
        $fetchUsers = DB::table('users')->where('username','=',Session()->get('USER_ID'))->get();
        $fetchContacts = DB::table('contact')->where('username','=',Session()->get('USER_ID'))->get();
        foreach($fetchUsers as $statusCheck){
            if($statusCheck->status == 2){
                Session()->flush('USER_ID');
                Session()->flush('USER_DP');
                return redirect('/user/verify')->with('message','Your Email is unverified we have sent you the Re-Verification Email Kindly check you registered email.');
                exit();
            }if($statusCheck->status == 3){
                Session()->flush('USER_ID');
                Session()->flush('USER_DP');
                return redirect('/user/login')->with('message','Your Account is disabled by the administrator');
                exit();
            }
    
        }
        return view('/user/dashboard',[
                                            
                                            'fetchTotalContactCount'=>$fetchTotalContactCount,
                                            'fetchTotalVisitorsCount'=>$fetchTotalVisitorsCount,
                                            'fetchUsers'=>$fetchUsers,
                                            'fetchContacts'=>$fetchContacts
                                            ]);
     }

     function setting(Request $request){
        $valudator = $request->validate([
            'username'=>'required',
            'nPassword'=>'required',
            'cPassword'=>'required'
       ]);
       
    $username = $request->input('username');
    $nPassword = $request->input('nPassword');
    $cPassword = $request->input('cPassword');
    $p = Hash::make($nPassword);
    $check = DB::table('users')
                ->where('username','=',$username)
                ->first();
    if($check!=null){
        
        if(Hash::check($cPassword,$check->password)){
            Session()->flush('USER_ID');
            Session()->flush('USER_DP');
            DB::table('user')->where('username','=',$username)->update(['password'=>$p]);
            return redirect('/user/login')->with('message','Password Changed Successfully Please relogin');
            
        }else{
            Session()->flush('USER_ID');
            Session()->flush('USER_DP');
            return redirect('/user/login')->with('message','Current Password is wrong. Fir se Try kejiye.');
        }
        
    }else{
        
        Session()->flush('USER_ID');
        Session()->flush('USER_DP');
        return redirect('/user/dashboard')->with('message','Please Try Again');
    }
    }

    function userStatus(Request $request,$id,$type){
        
        
                DB::table('users')->where('id','=',$id)->update(['status' => $type]);
                return redirect('/user/dashboard')->with('message','Profile Status Updated Successfully');
            
        
    }

    function getBasicDetails(){
        $fetchUsers = DB::table('users')->where('username','=',Session()->get('USER_ID'))->get();
        return view('/user/basic',[
            'fetchUsers'=>$fetchUsers
            ]);
    }
    function postBasicDetails(Request $request){
        $validate = $request->validate([
                    'name'=> 'required',
                    'dob'=> 'required',
                    'designation'=> 'required',
                    'about'=> 'required',
                    'goals'=> 'required',
                    'address'=> 'required',
                    'language'=> 'required',
                    'theme'=> 'required'
        ]);
        $name = $request->input('name');
        $dob = $request->input('dob');
        $designation = $request->input('designation');
        $about = $request->input('about');
        $goals = $request->input('goals');
        $address = $request->input('address');
        $language = $request->input('language');
        $theme = $request->input('theme');
        $facebook = $request->input('facebook');
        $insta = $request->input('insta');
        $gPlus = $request->input('gPlus');
        $twitter = $request->input('twitter');
        
        $updateBasic = DB::table('users')->where('username','=',Session()->get('USER_ID'))->update([

                    'name'=> $name,
                    'dob'=> $dob,
                    'designation'=> $designation,
                    'about'=> $about,
                    'goals'=> $goals,
                    'address'=> $address,
                    'language'=> $language,
                    'theme'=> $theme,
                    'facebook'=> $facebook,
                    'insta'=> $insta,
                    'gPlus'=> $gPlus,
                    'twitter'=> $twitter

        ]);
        if($updateBasic!=null){

            return redirect('/user/basic')->with('message','Information Updated Successfully');
        }else{
            return redirect('/user/basic')->with('message','There is nothing to update');
        }
    }
    function getSkills(){
        $fetchSkills = DB::table('skills')->where('username','=',Session()->get('USER_ID'))->get();
        return view('/user/skills',[
            'fetchSkills'=>$fetchSkills
            ]);
    }

    function postSkills(Request $request){
        $validate = $request->validate([
                    'skillName'=> 'required',
                    'skillProfencicy'=> 'required'
        ]);
        $skillName = $request->input('skillName');
        $skillProfencicy = $request->input('skillProfencicy');
        
        
        $updateSkills = DB::table('skills')->insert([
                    'username'=>Session()->get('USER_ID'),
                    'skillName'=> $skillName,
                    'skillProfencicy'=> $skillProfencicy
                    
        ]);
        if($updateSkills!=null){

            return redirect('/user/skills')->with('message','Information Updated Successfully');
        }else{
            return redirect('/user/skills')->with('message','There is nothing to update');
        }
    }

    function getExperience(){
        $fetchExperience = DB::table('experience')->where('username','=',Session()->get('USER_ID'))->get();
        return view('/user/experience',[
            'fetchExperience'=>$fetchExperience
            ]);
    }

    function postExperience(Request $request){
        $validate = $request->validate([
                    'designation'=> 'required',
                    'duration'=> 'required',
                    'companyTitle'=> 'required',
                    'details'=> 'required'
        ]);
        $designation = $request->input('designation');
        $duration = $request->input('duration');
        $companyTitle = $request->input('companyTitle');
        $details = $request->input('details');
        
        
        $updateExperience = DB::table('experience')->insert([
                    'username'=>Session()->get('USER_ID'),
                    'designation'=> $designation,
                    'duration'=> $duration,
                    'companyTitle'=> $companyTitle,
                    'details'=> $details
                    
        ]);
        if($updateExperience!=null){

            return redirect('/user/experience')->with('message','Information Updated Successfully');
        }else{
            return redirect('/user/experience')->with('message','There is nothing to update');
        }
    }

    function getEducation(){
        $fetchEducation = DB::table('education')->where('username','=',Session()->get('USER_ID'))->get();
        return view('/user/education',[
            'fetchEducation'=>$fetchEducation
            ]);
    }

    function postEducation(Request $request){
        $validate = $request->validate([
                    'degreeTitle'=> 'required',
                    'duration'=> 'required',
                    'college'=> 'required',
                    'details'=> 'required'
        ]);
        $degreeTitle = $request->input('degreeTitle');
        $duration = $request->input('duration');
        $college = $request->input('college');
        $details = $request->input('details');
        
        
        $updateEducation = DB::table('education')->insert([
                    'username'=>Session()->get('USER_ID'),
                    'degreeTitle'=> $degreeTitle,
                    'duration'=> $duration,
                    'college'=> $college,
                    'details'=> $details
                    
        ]);
        if($updateEducation!=null){

            return redirect('/user/education')->with('message','Information Updated Successfully');
        }else{
            return redirect('/user/education')->with('message','There is nothing to update');
        }
    }

    function getPortfolio(){
        $fetchPortfolio = DB::table('portfolio')->where('username','=',Session()->get('USER_ID'))->get();
        return view('/user/portfolio',[
            'fetchPortfolio'=>$fetchPortfolio
            ]);
    }

    function postPortfolio(Request $request){
        $validate = $request->validate([
                    'portImage'=> 'required|image|mimes:jpeg,png,jpg|max:1024',
                    'portTitle'=> 'required',
                    'details'=> 'required'
        ]);
        $portTitle = $request->input('portTitle');
        $details = $request->input('details');
        
        $permitted_chars = '0123456789';
        $uniqueString = substr(str_shuffle($permitted_chars), 0, 6);
        $imageName = time().'.'.$request->portImage->extension();  
        $url = $request->portImage->move(public_path('images'), $imageName);
       
        $img = Image::make($url);
        $img->resize(520, 300)->save();
        $updatePortfolio = DB::table('portfolio')->insert([
                    'username'=>Session()->get('USER_ID'),
                    'portTitle'=> $portTitle,
                    'portImage'=> $imageName,
                    'details'=> $details
                    
        ]);
        if($updatePortfolio!=null){

            return redirect('/user/portfolio')->with('message','Information Updated Successfully');
        }else{
            return redirect('/user/portfolio')->with('message','There is nothing to update');
        }
    }





    function userpassword(Request $request){
        $valudator = $request->validate([
            'username'=>'required',
            'nPassword'=>'required',
            'cPassword'=>'required'
       ]);
       
    $username = $request->input('username');
    $nPassword = $request->input('nPassword');
    $cPassword = $request->input('cPassword');
    $p = Hash::make($nPassword);
    $check = DB::table('users')
                ->where('username','=',$username)
                ->first();
    if($check!=null){
        
        if(Hash::check($cPassword,$check->password)){
            Session()->flush('USER_ID');
            DB::table('users')->where('username','=',$username)->update(['password'=>$p]);
            return redirect('/user/login')->with('message','Password Changed Successfully Please relogin');
            
        }else{
            Session()->flush('USER_ID');
            return redirect('/user/login')->with('message','Current Password is wrong. Fir se Try kejiye.');
        }
        
    }else{
        
        Session()->flush('USER_ID');
        return redirect('/user/dashboard')->with('message','Please Try Again');
    }
    }


function fetchSetting(){
        
        
    $fetchSetting = DB::table('users')->where('username','=',Session()->get('USER_ID'))->get();
        return view('/user/setting',[
            'fetchSetting'=>$fetchSetting
            ]);


}
function settingUpdate(Request $request){
        $validate = $request->validate([
            'email'=>'required',
            'phone'=>'required'
        ]);
        $email = $request->input('email');
        $phone = $request->input('phone');
        $checkEmail = DB::table('users')->where([['email',$email],['username','!=',Session()->get('USER_ID')]])->count();
        if($checkEmail >=1){
            return redirect('/user/setting')->with('message','Email Already Exists');
            exit();
        }
        $checkPhone = DB::table('users')->where([['phone',$phone],['username','!=',Session()->get('USER_ID')]])->count();
        if($checkPhone >=1){
            return redirect('/user/setting')->with('message','Phone Number Already Exists');
            exit();
        }
        if($request->hasFile('dp')){
            $dp = $request->input('dp');
            $permitted_chars = '0123456789';
        $uniqueString = substr(str_shuffle($permitted_chars), 0, 6);
        $imageName = time().'.'.$request->dp->extension();  
        $url = $request->dp->move(public_path('images'), $imageName);
       
        $img = Image::make($url);
        $img->resize(100, 100)->save();
            DB::table('users')->where('username','=',Session()->get('USER_ID'))->update([
                'email'=>$email,
                'phone'=>$phone,
                'dp'=>$imageName
            ]);
            Session()->flush('USER_DP');
        return redirect('/user/login')->with('message','Information Updated Successfully!. Please Relogin');

        }else{
            DB::table('users')->where('username','=',Session()->get('USER_ID'))->update([
                'email'=>$email,
                'phone'=>$phone
            ]);
        return redirect('/user/setting')->with('message','Information Updated Successfully');
        }
    


}




    function userCrud(Request $request,$id,$type){
        
        if($type=='skills'){
            DB::table('skills')->where('id','=',$id)->delete();
                return redirect('/user/skills')->with('message','Deleted Successfully ');
        }else if($type=='experience'){
            DB::table('experience')->where('id','=',$id)->delete();
                return redirect('/user/experience')->with('message','Deleted Successfully ');
        }else if($type=='education'){
            DB::table('education')->where('id','=',$id)->delete();
                return redirect('/user/education')->with('message','Deleted Successfully ');
        }else if($type=='portfolio'){
            DB::table('portfolio')->where('id','=',$id)->delete();
                return redirect('/user/portfolio')->with('message','Deleted Successfully ');
        }
        
    

}






    }
