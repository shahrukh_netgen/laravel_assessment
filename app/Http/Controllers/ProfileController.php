<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    function fetchData($username){
        $checkUsr = DB::table('users')->where('username','=',$username)->get()->count();
        if($checkUsr===0){
            return abort(404);
               exit();
        }
        $fetchUser = DB::table('users')->where('username','=',$username)->get();
        $fetchSkills = DB::table('skills')->where('username','=',$username)->get();
        $fetchPortfolio = DB::table('portfolio')->where('username','=',$username)->get();
        $fetchExperience = DB::table('experience')->where('username','=',$username)->get();
        $fetchEducation = DB::table('education')->where('username','=',$username)->get();
        
        foreach ($fetchUser as $theme){
            if($theme->status=="0" || $theme->status=="2" || $theme->status=="3"){
               return view('/layout/profileStatus');
               exit(); 
            }
            if($theme->theme=="t1"){
                return view('layout/theme1Base',[
                    'fetchUser'=>$fetchUser,
                    'fetchSkills'=>$fetchSkills,
                    'fetchPortfolio'=>$fetchPortfolio,
                    'fetchExperience'=>$fetchExperience,
                    'fetchEducation'=>$fetchEducation
                ]);
            }else if($theme->theme=="t2"){
                return view('layout/theme2Base',[
                    'fetchUser'=>$fetchUser,
                    'fetchSkills'=>$fetchSkills,
                    'fetchPortfolio'=>$fetchPortfolio,
                    'fetchExperience'=>$fetchExperience,
                    'fetchEducation'=>$fetchEducation
                ]);
            }else{
                return view('layout/theme1Base',[
                    'fetchUser'=>$fetchUser,
                    'fetchSkills'=>$fetchSkills,
                    'fetchPortfolio'=>$fetchPortfolio,
                    'fetchExperience'=>$fetchExperience,
                    'fetchEducation'=>$fetchEducation
                ]);
            }
        }
    
    }


    function contact(Request $request){
        $validate = $request->validate([
            'name'=>'required',
            'subject'=>'required',
            'email'=>'required|email',
            'details'=>'required',
            'username'=>'required'
        ]);
        $name = $request->input('name');
        $subject = $request->input('subject');
        $email = $request->input('email');
        $details = $request->input('details');
        $username = $request->input('username');
        $address = $request->input('address');
        $phone = $request->input('phone');
        $pushQuery = DB::table('contact')->where('username','=',$username)->insert([
            'username'=>$username,
            'name'=>$name,
            'subject'=>$subject,
            'email'=>$email,
            'details'=>$details,
            'address'=>$address,
            'phone'=>$phone
        ]);
        if($pushQuery!=null){
            return redirect('/'.$username)->with('message','Your Query Submitted Successfully');
        }else{
            return redirect('/'.$username)->with('message','Please Try Again');
        }
    }


}


