<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    function login(Request $request){
        $valudator = $request->validate([
                 'username'=>'required',
                 'password'=>'required'
            ]);
            
         $username = $request->input('username');
         $password = $request->input('password');
         
         $check = DB::table('admin')
                     ->where('username','=',$username)
                     ->first();
         if($check!=null){
             
             if(Hash::check($password,$check->password)){
                 $request->session()->put('ADMIN_ID',$check->username);
                 return redirect('/admin/dashboard');
                 
             }else{
              
                 return redirect('/admin/login')->with('message','Password galat hai. Fir se Try kejiye.');
             }
             
         }else{
             
             
             return redirect('/admin/login')->with('message','Please Try Again');
         }
     }

     function fetchDashboardData(){
        
        $fetchUserCount = DB::table('users')->count();
        $fetchTotalContactCount = DB::table('contact')->count();
        $fetchTotalVisitorsCount = DB::table('visitors')->count();
        $fetchUsers = DB::table('users')->get();
        
        return view('/admin/dashboard',[
                                            'fetchUserCount'=>$fetchUserCount,
                                            'fetchTotalContactCount'=>$fetchTotalContactCount,
                                            'fetchTotalVisitorsCount'=>$fetchTotalVisitorsCount,
                                            'fetchUsers'=>$fetchUsers
                                            ]);
     }

     function setting(Request $request){
        $valudator = $request->validate([
            'username'=>'required',
            'nPassword'=>'required',
            'cPassword'=>'required'
       ]);
       
    $username = $request->input('username');
    $nPassword = $request->input('nPassword');
    $cPassword = $request->input('cPassword');
    $p = Hash::make($nPassword);
    $check = DB::table('admin')
                ->where('username','=',$username)
                ->first();
    if($check!=null){
        
        if(Hash::check($cPassword,$check->password)){
            Session()->flush('ADMIN_ID');
            DB::table('admin')->where('username','=',$username)->update(['password'=>$p]);
            return redirect('/admin/login')->with('message','Password Changed Successfully Please relogin');
            
        }else{
            Session()->flush('ADMIN_ID');
            return redirect('/admin/login')->with('message','Current Password is wrong. Fir se Try kejiye.');
        }
        
    }else{
        
        Session()->flush('ADMIN_ID');
        return redirect('/admin/dashboard')->with('message','Please Try Again');
    }
    }

    function adminCrud(Request $request,$id,$type){
        
        
                DB::table('users')->where('id','=',$id)->update(['status' => $type]);
                return redirect('/admin/dashboard')->with('message','Profile Status Updated Successfully');
            
        
    }










    }
