<!doctype html>
<html lang="en">
  <head>
  	<title>User Verification: Resume App</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="{{asset('/loginAssets/css/style.css')}}">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

	</head>
	<body class="img js-fullheight" style="background-image: url({{asset('/loginAssets/images/bg.jpg')}}); background-repeat: no-repeat !important;
  background-size: auto;">
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4">
					<div class="login-wrap p-0">
		      	<h3 class="mb-4 text-center">Verify Your Email!</h3>
                  <form action="{{url('/userVerify')}}" method="post" enctype="multipart/form-data" class="signin-form">
                  @csrf
		      	    	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif              
@if(Session()->has('message'))
<p class="alert alert-danger">{{Session()->get('message')}}</p>
@endif
                  <div class="form-group">
		      			<input type="text" name="vCode" class="form-control" placeholder="Enter Verification Code" required>
		      		</div>
		      	
	            <div class="form-group">
	            	<button type="submit" class="form-control btn btn-primary submit px-3">Verify Now</button>
	            </div>
	            <div class="form-group d-md-flex">
	            	<div class="w-50">
		            	
								</div>
								<div class="w-50 text-md-right">
									
								</div>
	            </div>
	          </form>
	          
	          
		      </div>
				</div>
			</div>
		</div>
	</section>

	
  <script src="{{asset('/loginAssets/js/popper.js')}}"></script>
  <script src="{{asset('/loginAssets/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('/loginAssets/js/main.js')}}"></script>
  <script>
    $(function () {
        $("#dob").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-100:+0'
        });
    });
</script>
	</body>
</html>

