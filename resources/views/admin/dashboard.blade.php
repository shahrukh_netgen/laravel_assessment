@extends('layout.base')
@section('titlePage','Admin | Dashboard, Resume App')
@section('container')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Welcome {{session()->get('ADMIN_ID')}}!</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/admin/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row"><div class="col-lg-12">
              <div class="card">
                  <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Summary</h3>
                  
                </div>
              </div>
              <div class="card-body">
            <div class="row">     
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-user-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Users</span>
              <span class="info-box-number">{{$fetchUserCount}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Contact Queries</span>
              <span class="info-box-number">{{$fetchTotalContactCount}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fas fa-globe-asia"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Visitors</span>
              <span class="info-box-number">{{$fetchTotalVisitorsCount}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        </div> 
        
              </div>
              </div>
          </div></div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">User Profiles</h3>
                  @if(Session()->has('message'))
<p class="alert alert-success">{{Session()->get('message')}}</p>
@endif
                </div>
              </div>
              <div class="card-body">
                
              <table id="contactMessages" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                  <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Designation </th>
                    <th>Language </th>
                    <th>Theme </th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($fetchUsers as $usrs)
                  <tr>
                      <td><a href='{{url("/$usrs->id")}}' target="_BLANK">{{$usrs->name}}</a></td>
                      <td>{{$usrs->email}}</td>
                      <td>{{$usrs->phone}}</td>
                      <td>{{$usrs->designation}}</td>
                      <td>{{$usrs->language}}</td>
                      <td>{{$usrs->theme}}</td>
                      @if($usrs->status==1)
                      <td><a href='{{url("/admin/crud/$usrs->id&3")}}' class='btn btn-danger addon'>De-Activate</a></td>
                      @else
                      <td><a href='{{url("/admin/crud/$usrs->id&1")}}' class='btn btn-primary addon'>Activate</a></td>
                      @endif
                  </tr>
                  @endforeach
                
                 
             
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Designation </th>
                    <th>Language </th>
                    <th>Theme </th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
                
              </div>
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
          
        </div>
        <!-- /.row -->
        
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
@endsection