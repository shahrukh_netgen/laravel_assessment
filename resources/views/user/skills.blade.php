@extends('layout.baseUser')
@section('titlePage','User | Skills Details, Resume App')
@section('container')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Hello {{session()->get('USER_ID')}}! Update Your Skills Below.</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/admin/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Manage Portfolio</li>
              <li class="breadcrumb-item active">Skills</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        
        


        <div class="row">
        <div class="col-lg-6">
            
            
            <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Skills Details</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        <form action="{{url('/user/skills/post_url')}}" method="post" enctype="multipart/form-data" class="signin-form">
				  @csrf
		      	    	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 
@if(Session()->has('message'))
<p class="alert alert-danger">{{Session()->get('message')}}</p>
@endif
             <div class="form-group">
              <label for="field">Skill Name</label>
              <input type="text" class="form-control" name="skillName" placeholder="eg. HTML, CSS etc.." required>
            </div>
            <div class="form-group">
              <label for="field">Skill Profeciency(1-100)</label>
              <input type="text" class="form-control" name="skillProfencicy" placeholder="Eg: 90 etc.." required>
            </div>
            
            
          
            <button class="btn btn-primary addon" id="skill update">Save</button>
          
            </form>
            
        </div>
        <!-- /.card-body -->
      </div>
    </div>
    <!-- /.col-md-6 -->
    <div class="col-lg-6">
            
            
			  <div class="card">
              <div class="card-header">
                <h3 class="card-title">Skill  List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                 <table id="categoryAdd" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                  <th>Skill Name</th>
                    <th>Skill Profeciency</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                @foreach($fetchSkills as $skills)
                <tr>
                    <td>{{$skills->skillName}}</td>
                    <td>{{$skills->skillProfencicy}}</td>
                    <td><a href='{{url("/user/crud/$skills->id&skills")}}' class='btn btn-danger addon'>Delete</a></td>
                </tr>
                @endforeach
                 
             
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>Skill Name</th>
                    <th>Skill Profeciency</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col-md-6 -->
          
        </div>
        <!-- /.row -->
        
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
@endsection