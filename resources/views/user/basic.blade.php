@extends('layout.baseUser')
@section('titlePage','User | Basic Details, Resume App')
@section('container')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/user/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item">Manage Portfolio</li>
              <li class="breadcrumb-item active">Basic Details</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        
        


        <div class="row">
        <div class="col-lg-12">
            
            
            <div class="card">
        <div class="card-header">
          <h3 class="card-title">Update Basic Details</h3>
        </div>
        <!-- /.card-header -->
        <form action="{{url('/user/basic/post_url')}}" method="post" enctype="multipart/form-data" class="signin-form">
				  @csrf
		      	    	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 
@if(Session()->has('message'))
<p class="alert alert-danger">{{Session()->get('message')}}</p>
@endif
        <div class="card-body">
        @foreach ($fetchUsers as $usrs)
        <div class="form-group">
                    <label for="field">Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Your Name"  value="{{$usrs->name}}" required>
                  </div>
                  <div class="form-group">
                    <label for="field">Date of Birth</label>
                    <input type="text" class="form-control" placeholder="DD-MM-YYYY" name="dob" id="dob" value="{{$usrs->dob}}" required>
                  </div>
        <div class="form-group">
              <label for="field">Your Designation</label>
              <input type="text" class="form-control" name="designation" placeholder="eg. PHP Developer etc.." value="{{$usrs->designation}}" required>
            </div>


             <div class="form-group">
              <label for="field">Your Profile Description</label>
              <textarea class="form-control" name="about" placeholder="Enter Your Profile Description"  required>{{$usrs->about}}</textarea>
            </div>
            <div class="form-group">
              <label for="field">Your Goals</label>
              <textarea class="form-control" name="goals" placeholder="Your Goals"  required>{{$usrs->goals}}</textarea>
            </div>
            <div class="form-group">
              <label for="field">Your Address</label>
              <textarea class="form-control" name="address" placeholder="Full Address"  required>{{$usrs->address}}</textarea>
            </div>
            <div class="form-group">
              <label for="field">Language You Speak</label>
              <input type="text" class="form-control" name="language" placeholder="eg. English, Hindi" value="{{$usrs->language}}" required>
            </div>
            <div class="form-group">
              <label for="field">Facebook Profile</label>
              <input type="text" class="form-control" name="facebook" value="{{$usrs->facebook}}" placeholder="https://fb.com/username" >
            </div>
            <div class="form-group">
              <label for="field">Instagram Profile</label>
              <input type="text" class="form-control" name="insta" value="{{$usrs->insta}}" placeholder="https://instagram.com/username" >
            </div>
            <div class="form-group">
              <label for="field">Google+ Profile</label>
              <input type="text" class="form-control" name="gPlus" value="{{$usrs->gPlus}}" placeholder="https://phus.google.com/username" >
            </div>
            <div class="form-group">
              <label for="field">Twitter Profile</label>
              <input type="text" class="form-control" name="twitter" value="{{$usrs->twitter}}" placeholder="https://twitter.com/username" >
            </div>
            <div class="form-group">
                  <label for="field">Select Your Theme</label>
                  <select class="form-control" name="theme" required>
                    <option value="{{$usrs->theme}}">{{$usrs->theme}}</option>
                    <option>------------</option>
                    <option value="t1">t1</option>
                    <option value="t2">t2</option>
                  </select>

                  </div>
                  @endforeach
            <button class="btn btn-primary addon" id="educationUpdate">Save</button>
          
            
            
        </div>
        <!-- /.card-body -->
        </form>
      </div>
    </div>
    <!-- /.col-md-6 -->
    
          
        </div>
        <!-- /.row -->
        
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <script>
    $(function () {
        $("#dob").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '-100:+0'
        });
    });
</script>
@endsection