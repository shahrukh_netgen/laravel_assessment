@extends('layout.baseUser')
@section('titlePage','Setting | Resume App')
@section('container')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Account Setting</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/user/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Account Setting</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          
              
        <div class="row">
            <div class="col-lg-3">
              <div class="card">
                  <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Change Password</h3>
                  
                </div>
              </div>
              <div class="card-body">
                  @if(Session()->has('message'))
<p class="alert alert-{{Session()->get('type')}}">{{Session()->get('message')}}</p>
@endif
              <form action='{{url("/user/setting/password/setting_url")}}' method="POST" enctype="multipart/form-data">
              @csrf
              <span id="msg"></span>
              <div class="form-group">
                    <label for="field">Current Password</label>
                    <input type="password" class="form-control" name="cPassword" value="" required>
                    <input type="hidden" class="form-control" name="username" value="{{Session()->get('USER_ID')}}" required>
                </div>
                <div class="form-group">
                    <label for="field">New Password</label>
                    <input type="password" class="form-control" name="nPassword" value="" required>
                    
                </div>
                
                <button type="submit"  class="btn btn-primary addon" id="categorySave">Save</button>
              </div>
              </form>
              </div>
          </div>
          <div class="col-lg-9">
            
            
			  <div class="card">
              <div class="card-header">
                <h3 class="card-title">BLANK</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                 
                 <form action="{{url('/user/setting/update')}}" method="post" enctype="multipart/form-data">
                 @csrf
		      	    	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 
@if(Session()->has('message'))
<p class="alert alert-danger">{{Session()->get('message')}}</p>
@endif
@foreach($fetchSetting as $s)
<div class="form-group">
                    <label for="field">Email</label>
                    <input type="text" class="form-control" name="email" placeholder="Your Email"  value="{{$s->email}}" required>
                  </div>
                  <div class="form-group">
                    <label for="field">Phone Number</label>
                    <input type="text" class="form-control" placeholder="Your Contact Number" name="phone" value="{{$s->phone}}" required>
                  </div>
        <div class="form-group">
              <label for="field">Choose Profile Picture</label>
              <input type="file" class="form-control" name="dp" placeholder="Choose a Profile Picture" >
            </div>
            @endforeach
            <button class="btn btn-primary addon" id="educationUpdate">Save</button>
                 </form>
                 
                 
                 
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col-md-6 -->
          
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>


  @endsection