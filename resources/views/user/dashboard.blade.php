@extends('layout.baseUser')
@section('titlePage','User | Dashboard, Resume App')
@section('container')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Welcome {{session()->get('USER_ID')}}!</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/user/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row"><div class="col-lg-12">
              <div class="card">
                  <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Summary</h3>
                  
                </div>
              </div>
              <div class="card-body">
            <div class="row">     
          
        
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Contact Queries</span>
              <span class="info-box-number">{{$fetchTotalContactCount}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fas fa-globe-asia"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Visitors</span>
              <span class="info-box-number">{{$fetchTotalVisitorsCount}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fas fa-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Profile Status</span>
              <table>
              @foreach($fetchUsers as $usrs)
                  <tr>
                      <td><a href='{{url("/$usrs->username")}}' target="_BLANK">{{$usrs->name}}</a></td>
                      
                      
                      @if($usrs->status==1)
                      <td><a href='{{url("/user/status/$usrs->id&0")}}' class='btn btn-danger addon'>De-Activate</a></td>
                      @else
                      <td><a href='{{url("/user/status/$usrs->id&1")}}' class='btn btn-primary addon'>Activate</a></td>
                      @endif
                  </tr>
                  @endforeach
              </table>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        


        </div> 
        
              </div>
              </div>
          </div></div>
        <div class="row">
         
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Contact Query</h3>
                  
                </div>
              </div>
              <div class="card-body">
                
              <table id="contactMessages" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                  <th>Subject</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th>Details</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($fetchContacts as $cntcts)
                  <tr>
                      <td>{{$cntcts->subject}}</td>
                      <td>{{$cntcts->name}}</td>
                      <td>{{$cntcts->email}}</td>
                      <td>{{$cntcts->phone}}</td>
                      <td>{{$cntcts->address}}</td>
                      <td>{{$cntcts->details}}</td>
                      
                      
                  </tr>
                  @endforeach
                
                 
             
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>Subject</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th>Details</th>
                  </tr>
                  </tfoot>
                </table>
                
              </div>
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
        
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
@endsection