@foreach($fetchUser as $basic)
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Resume | {{$basic->name}}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="C-Resume a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- css -->
<!-- font-awesome icons -->
<link rel="stylesheet" href="{{asset('publicAssets/theme2/css/font-awesome.min.css')}}" />
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="{{asset('publicAssets/theme2/css/style.css')}}" type="text/css" media="all" />
<link rel="stylesheet" href="{{asset('publicAssets/theme2/css/bootstrap.min.css')}}" type="text/css" media="all" />
<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="{{asset('publicAssets/theme2/js/jquery-2.1.4.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('publicAssets/theme2/js/bootstrap.min.js')}}"></script>
<!-- //Default-JavaScript-File -->

</head>
<body>
<!-- banner -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 
@if(Session()->has('message'))
<p class="alert alert-danger">{{Session()->get('message')}}</p>
@endif
<div class="w3-banner-top">
	<div class="agileinfo-dot">
			<div class="w3layouts_menu">
				<nav class="cd-stretchy-nav edit-content">
					<a class="cd-nav-trigger" href="#0"> Menu <span aria-hidden="true"></span> </a>
					<ul>
						<li><a href="#home" class="scroll"><span>Home</span></a></li>
						
						<li><a href="#experiences" class="scroll"><span>Experiences</span></a></li>
						<li><a href="#skills" class="scroll"><span>Skills</span></a></li> 
						<li><a href="#projects" class="scroll"><span>Projects</span></a></li>
						<li><a href="#contact" class="scroll"><span>Contact</span></a></li>
					</ul> 
					<span aria-hidden="true" class="stretchy-nav-bg"></span>
				</nav> 
			</div>

		<div class="w3-banner-grids">
			<div class="col-md-6 w3-banner-grid-left">
				<div class="w3-banner-img">
					<img src='{{url("/images/$basic->dp")}}' alt="img">
					<h3 class="test">{{$basic->name}}</h3>
					<p class="test" >{{$basic->designation}}</p>
				</div>
			</div>
			<div class="col-md-6 w3-banner-grid-right">
			<div class="w3-banner-text">
				<h3>Career Goal</h3>
				<p>{{$basic->goals}}</p>
			</div>
				<div class=" w3-right-addres-1">
				<ul class="address">
								<li>
									<ul class="agileits-address-text ">
										<li class="agile-it-adress-left"><b>D.O.B</b></li>
										<li><span>:</span>{{$basic->dob}}</li>
									</ul>
								</li>
								<li>
									<ul class="agileits-address-text">
										<li class="agile-it-adress-left"><b>PHONE</b></li>
										<li><span>:</span>{{$basic->phone}}</li>
									</ul>
								</li>
								<li>
									<ul class="agileits-address-text">
										<li class="agile-it-adress-left"><b>ADDRESS</b></li>
										<li><span>:</span>{{$basic->address}}</li>
									</ul>
								</li>
								<li>
									<ul class="agileits-address-text">
										<li class="agile-it-adress-left"><b>E-MAIL</b></li>
										<li><span>:</span><a href="mailto:example@mail.com"> {{$basic->email}}</a></li>
									</ul>
								</li>
								<li>
									
								</li>
							</ul> 

				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		</div>
		<div class="thim-click-to-bottom">
				<a href="#about" class="scroll">
					<i class="fa  fa-chevron-down"></i>
				</a>
			</div>

	</div>
<!-- banner -->
<!-- /about -->


</div>
<!-- //about  -->
<!--/ education -->

<div class="w3-edu-top" id="experiences">
	<div class="container">
		<div class="w3-edu-grids">
        
			<div class="col-md-6 w3-edu-grid-left">
				<div class="w3-edu-grid-header">
				<h3>Experiences</h3>
				</div>
                @foreach($fetchExperience as $experience)
				<div class="col-md-4 w3-edu-info1">
					<h3>{{$experience->duration}}</h3>
					<h4>{{$experience->designation}}</h4>
			</div>
			<div class="col-md-6 w3-edu-info2">
				<h3>{{$experience->companyTitle}}</h3>
					<h4><i class="fa fa-users" aria-hidden="true"></i><span>{{$experience->designation}}</span></h4>
					<p>{{$experience->details}}</p>
			</div>
			<div class="clearfix"></div>
			@endforeach
			
			
					
			</div>
            
            
			<div class="col-md-6 w3-edu-grid-right">
			<div class="w3-edu-grid-header">
			<h3>Education</h3>
			</div>
            @foreach($fetchEducation as $education)
				<div class="col-md-3 w3-edu-info-right1">
					<h3>{{$education->duration}}</h3>
			</div>
			<div class="col-md-9 w3-edu-info-right2">
				<h3>{{$education->college}}</h3>
					<h4>{{$education->degreeTitle}}</h4>
					<p>{{$education->details}}</p>
			</div>
			<div class="clearfix"></div>
			@endforeach
			
					
		</div>
		<div class="clearfix"></div>	
	</div>
	</div>
	</div>

    <!-- //education -->
<!-- skills -->

<div class="skills" id="skills">
	<div class="container">
	<div class="title-w3ls">
		<h4>My Skills</h4>
		</div>
		<div class="skills-bar">
            @foreach($fetchSkills as $skills)
		<div class="col-md-6 w3-agile-skills-grid">
			<section class='bar'>
				<section class='wrap'>
					<div class='wrap_right'>
					  <div class='bar_group'>
						<div class='bar_group__bar thin' label='{{$skills->skillName}}' show_values='true' tooltip='true' value='{{$skills->skillProfencicy}}'></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</section>
			</section>
		</div>
		
        @endforeach
		<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //skills -->

<!-- main-content -->
<div class="main-content">
		<!-- gallery -->
	<div class="gallery" id="projects">
		<div class="w3-gallery-head">
			<h3>My projects</h3>
		</div>
	<div class="container">
		<div class="gallery_gds">
			<ul class="simplefilter ">
                <li class="active" data-filter="all">All</li>
                
            </ul>   
            <div class="filtr-container " style="padding: 0px; position: relative; height: 858px;">
				@foreach($fetchPortfolio as $portfolio)
                <div class="col-md-4 col-ms-6 jm-item first filtr-item" data-category="1, 5" data-sort="Busy streets" style="opacity: 1; transform: scale(1) translate3d(0px, 0px, 0px); backface-visibility: hidden; perspective: 1000px; transform-style: preserve-3d; position: absolute; transition: all 0.5s ease-out 0ms;">
					<div class="jm-item-wrapper">
						<div class="jm-item-image">
							<img src='{{url("/images/$portfolio->portImage")}}' alt="property" />
							<span class="jm-item-overlay"> </span>
							<div class="jm-item-button"><a href="#"  data-toggle="modal" data-target="#{{$portfolio->id}}">View Details</a></div>
						</div>	
						
					</div>
				</div>
                <div class="modal fade" id="{{$portfolio->id}}" tabindex="-1" role="dialog" >
				<div class="modal-dialog">
							<!-- Modal content-->
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
							<div class="w3ls-property-images w3-pop1-img">
							</div>
					
						<div class="ins-details">
							<div class="ins-name">
								<h3>{{$portfolio->portTitle}}</h3>
								<p>{{$portfolio->details}}</p>
								
							</div>
							
						</div>
						<div class="clearfix"></div>			
			     </div>
	</div>
				@endforeach

               <div class="clearfix"> </div>
            </div>
		</div>
	</div>	
	</div>
	<!--//gallery-->
	</div>
<!-- //main-content -->
	
					 
		 
					 

					 

					 


				 					
 <script src="{{asset('publicAssets/theme2/js/jquery.filterizr.js')}}"></script>
    
    <!-- Kick off Filterizr -->
    <script type="text/javascript">
        $(function() {
            //Initialize filterizr with default options
            $('.filtr-container').filterizr();
        });
    </script>

<!-- contact -->
<div class="contact" id="contact">
	<div class="container">
		<div class="w3ls-heading">
			<h3>Contact me</h3>
		</div>
			<div class="contact-w3ls">
				<form action="{{url('/public/contact')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-md-7 col-sm-7 contact-left agileits-w3layouts">
						<input type="text" name="name" placeholder="Name" required="">
						<input type="email"  class="email" name="email" placeholder="Email" required="">
						<input type="text" name="phone" placeholder="Mobile Number" required="">
						<!-- <input type="text" class="email" name="Last Name" placeholder="Last Name" required=""> -->
					</div> 
					<div class="col-md-5 col-sm-5 contact-right agileits-w3layouts">
						<textarea name="details" placeholder="Message" required=""></textarea>
                        <input type="hidden" name="address" value="none" />
                        <input type="hidden" name="subject" value="none" />
                        <input type="hidden" name="username" value="{{$basic->username}}" />
						<input type="submit" value="Submit">
					</div>
					<div class="clearfix"> </div> 
				</form>
			</div>  

	</div>
</div>
<!-- //contact -->
<!-- footer -->
	<div class="w3l_footer">
		<div class="container">
			
			<div class="w3ls_footer_grids">
				
				<div class="w3ls_footer_grid">
					<div class="col-md-4 w3ls_footer_grid_left">
						<div class="w3ls_footer_grid_leftl">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						</div>
						<div class="w3ls_footer_grid_leftr">
							<h4>Location</h4>
							<p>{{$basic->address}}</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="col-md-4 w3ls_footer_grid_left">
						<div class="w3ls_footer_grid_leftl">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<div class="w3ls_footer_grid_leftr">
							<h4>Email</h4>
							<a href="mailto:{{$basic->email}}">{{$basic->email}}</a>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="col-md-4 w3ls_footer_grid_left">
						<div class="w3ls_footer_grid_leftl">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</div>
						<div class="w3ls_footer_grid_leftr">
							<h4>Call Me</h4>
							<p>{{$basic->phone}}</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="w3l_footer_pos">
			<p>© 2017 C-Resume. All Rights Reserved | Design by <a href="https://w3layouts.com/">W3layouts</a></p>
		</div>
	</div>
<!-- //footer -->
<script src="{{asset('publicAssets/theme2/js/bars.js')}}"></script>
<!-- start-smoth-scrolling -->
<script src="{{asset('publicAssets/theme2/js/SmoothScroll.min.js')}}"></script>
<!-- text-effect -->
		<script type="text/javascript" src="{{asset('publicAssets/theme2/js/jquery.transit.js')}}"></script> 
		<script type="text/javascript" src="{{asset('publicAssets/theme2/js/jquery.textFx.js')}}"></script> 
		<script type="text/javascript">
			$(document).ready(function() {
					$('.test').textFx({
						type: 'fadeIn',
						iChar: 100,
						iAnim: 1000
					});
			});
		</script>
<!-- //text-effect -->
<!-- menu-js --> 	
	<script src="{{asset('publicAssets/theme2/js/modernizr.js')}}"></script>	
	<script src="{{asset('publicAssets/theme2/js/menu.js')}}"></script>
<!-- //menu-js --> 	


<script type="text/javascript" src="{{asset('publicAssets/theme2/js/move-top.js')}}"></script>

<script type="text/javascript" src="{{asset('publicAssets/theme2/js/easing.js')}}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>


</body>
</html>
@endforeach