@extends('layout.base')
@section('sidebar')
<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="{{url('/admin/post/list')}}" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>

              <p>
                Dashboard
                
              </p>
            </a>
            
          </li>
         
          
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Blogs
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/admin/post/add')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add New Blog</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/admin/post/edit" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit Blog</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/admin/post/category')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Category</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/admin/post/subCategory')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sub Category</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateEducation.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update Education Details</p>
                </a>
              </li>
            </ul>
          </li>
 		
			
          <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
			
           
           
			
			
       
        </ul>
      </nav>
      @endsection