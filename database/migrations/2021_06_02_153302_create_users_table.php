<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('name')->nullable();
            $table->string('designation')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('gPlus')->nullable();
            $table->string('insta')->nullable();
            $table->text('goals')->nullable();
            $table->text('about')->nullable();
            $table->string('age')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->text('address')->nullable();
            $table->string('language')->nullable();
            $table->string('dob')->nullable();
            $table->string('dp')->nullable();
            $table->string('status')->nullable()->default('0');
            $table->string('theme')->nullable()->default('t1');
            $table->string('password')->nullable();
            $table->string('vCode')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
