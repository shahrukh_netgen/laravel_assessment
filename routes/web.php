<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
  // return view('frontend/theme1/base');
//});
Route::domain('{username}.'.env('APP_URL'))->group(function(){
    Route::get('/', [ProfileController::class,'fetchData']);
});
Route::view('/','userLogin.login');
//Route::get('/{username}', [ProfileController::class,'fetchData']);

Route::post('/public/contact', [ProfileController::class,'contact']);
Route::view('/user/login','userLogin.login');
Route::view('/user/signup','userLogin.signup');
Route::view('/admin/login','adminLogin.login');
Route::view('/user/verify','userLogin.verify');
Route::post('/adminLogin',[AdminController::class,'login']);
Route::post('/userLogin',[UserController::class,'login']);
Route::post('/userSignup',[UserController::class,'signup']);
Route::post('/userVerify',[UserController::class,'verifyEmail']);
Route::get('/admin/logout', function(){
    if(Session()->has('ADMIN_ID')){
        Session()->flush('ADMIN_ID');
        return redirect('/admin/login')->with('message','You have logout Successfully');
    }
});
Route::get('/user/logout', function(){
    if(Session()->has('USER_ID')){
        Session()->flush('USER_ID');
        Session()->flush('USER_DP');
        return redirect('/user/login')->with('message','You have logout Successfully');
    }
});
Route::group(['middleware'=>['AdminMiddleware']], function(){

    Route::get('/admin/dashboard',[AdminController::class,'fetchDashboardData']);
    Route::view('/admin/setting','admin.setting');
    Route::post('/admin/setting/setting_url',[AdminController::class,'setting']);
    Route::get('/admin/crud/{id}&{type}',[AdminController::class,'adminCrud']);
    

});

Route::group(['middleware'=>['UserMiddleware']], function(){

    Route::get('/user/dashboard',[UserController::class,'fetchDashboardData']);
    Route::get('/user/basic',[UserController::class,'getBasicDetails']);
    Route::post('/user/basic/post_url',[UserController::class,'postBasicDetails']);
    Route::get('/user/skills',[UserController::class,'getSkills']);
    Route::post('/user/skills/post_url',[UserController::class,'postSkills']);
    Route::get('/user/experience',[UserController::class,'getExperience']);
    Route::post('/user/experience/post_url',[UserController::class,'postExperience']);
    Route::get('/user/education',[UserController::class,'getEducation']);
    Route::post('/user/education/post_url',[UserController::class,'postEducation']);
    Route::get('/user/portfolio',[UserController::class,'getPortfolio']);
    Route::post('/user/portfolio/post_url',[UserController::class,'postPortfolio']);
    Route::view('/user/setting','user.setting');
    Route::post('/user/setting/password/setting_url',[UserController::class,'userpassword']);
    Route::get('/user/crud/{id}&{type}',[UserController::class,'userCrud']);
    Route::get('/user/status/{id}&{type}',[UserController::class,'userStatus']);
    Route::post('/user/setting/update',[UserController::class,'settingUpdate']);
    Route::get('/user/setting',[UserController::class,'fetchSetting']);

});
