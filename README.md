## INSTALLATION PROCEDURE

## BASIC INFORMATION
#App Name: Resume App
#Project By: Shahrukh Sheikh
#Framework User: Laravel 8.X
#Last Updated: JUN-06-2021

## INSTALLATION STEPS
1. Make Database in Mysql
2. Update Database Name / username / password in .env file
3. Update APP_URL=yourdomain.com
4. Run Migrations

## USAGE GUIDE
#Admin Panel: __URL__/admin/login
- Username: admin
- Password: admin
#User Login: __URL__/user/login
#User Registration: __URL__/user/register
#Profile View: __URL__/username or username.__URL__

## IMPORTANT INFORMATION
- By Default Profile Status is set to 0
- Profile De-Activated by admin cannot be reactivated by end user
- To make the public profile visible and accessible, user must activate the profile.

